package ru.vladuska;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.logging.Level;

import java.lang.reflect.Constructor;

import static org.bukkit.Bukkit.getLogger;
import static ru.vladuska.Localization.effectTranslate;
import static ru.vladuska.Runner.bossBar;

public class Action {

    private Config config = Config.getInstance();

    private Player player;
    private boolean enchant; // true - positive, false - negative

    public Action(String playerName) {
        this.player = Bukkit.getPlayer(playerName);

        /*Bukkit.getServer().sendMessage(
                Component.text("Чат выбрал игрока, им становится ", NamedTextColor.DARK_PURPLE)
                    .append(Component.text(playerName, NamedTextColor.AQUA).decoration(TextDecoration.BOLD, true))
                    .append(Component.text("!", NamedTextColor.DARK_PURPLE))
        );*/

        for (Player player : Bukkit.getOnlinePlayers()) {
            player.sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§5§lЧат выбрал игрока - §2§l" + playerName));
        }
    }

    public String getPlayername() {
        return this.player.getName();
    }

    public void setEnchant(boolean enchant) {
        this.enchant = enchant;

        applyEnchant();
    }

    private void applyEnchant() {
        Effect potionEffectType = getRandomEffect();
        String effectName = "";

        for (int i = 0; i < potionEffectType.types.length; i++) {
            effectName += effectTranslate(potionEffectType.types[i]);

            if (i != potionEffectType.types.length - 1) {
                effectName += " и ";
            }
        }

        /*Bukkit.getServer().sendMessage(
                Component.text("Чат выбрал эффект, ", NamedTextColor.DARK_PURPLE)
                        .append(Component.text(player.getName(), NamedTextColor.AQUA).decoration(TextDecoration.BOLD, true))
                        .append(Component.text(" получает ", NamedTextColor.DARK_PURPLE))
                        .append(Component.text(effectName, enchant ? NamedTextColor.GREEN : NamedTextColor.RED).decoration(TextDecoration.BOLD, true))
                        .append(Component.text(" на " + potionEffectType.duration + " секунд!", NamedTextColor.DARK_PURPLE))
        );*/

        bossBar.setTitle("§5§lЧат выбрал эффект - §a§l" + player.getName() + " §5§lполучает " + (enchant ? "§2§l" : "§4§l") + effectName + " §5§lна §a§l" + (int) potionEffectType.duration + " §5§lсекунд!");
        bossBar.setProgress(0);
        bossBar.setVisible(true);

        String finalEffectName = effectName;
        new BukkitRunnable() {
            @Override
            public void run() {
                for (double i = 1; i <= potionEffectType.duration; i++) {
                    bossBar.setTitle("§5§lЧат выбрал эффект - §a§l" + player.getName() + " §5§lполучает " + (enchant ? "§2§l" : "§4§l") + finalEffectName + " §5§lна §a§l" + (int) (potionEffectType.duration - i) + " §5§lсекунд!");
                    bossBar.setProgress(i * (1 / potionEffectType.duration));
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                bossBar.setVisible(false);
                bossBar.setProgress(0);
            }
        }.runTaskAsynchronously(Bukkit.getPluginManager().getPlugin("TwitchChoice"));

        if (potionEffectType.types[0].equals(PotionEffectType.LUCK)) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    player.setAllowFlight(true);
                    player.setFlying(true);

                    try {
                        Thread.sleep((int) potionEffectType.duration * 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    player.setFlying(false);
                    player.setAllowFlight(false);
                }
            }.runTaskAsynchronously(Bukkit.getPluginManager().getPlugin("TwitchChoice"));
        } else {
            new BukkitRunnable() {
                @Override
                public void run() {
                    for (PotionEffectType type : potionEffectType.types) {
                        player.addPotionEffect(new PotionEffect(type, (int) potionEffectType.duration * 20,  5));
                    }
                }
            }.runTask(Bukkit.getPluginManager().getPlugin("TwitchChoice"));
        }
    }

    public void swap(String playername) {
        bossBar.setTitle("§5§lЧат выбрал свапнуть игроков - §a§l" + player.getName() + " §r§5§lсвапнется с... ");
        bossBar.setProgress(0);
        bossBar.setVisible(true);

        /*Bukkit.getServer().sendMessage(
                Component.text("Чат выбрал рандомно свапнуть игроков! ", NamedTextColor.DARK_PURPLE)
                        .append(Component.text(this.getPlayername(), NamedTextColor.AQUA).decoration(TextDecoration.BOLD, true))
                        .append(Component.text(" поменяется местами с... ", NamedTextColor.DARK_PURPLE))
        );*/

        Player secondPlayer = Bukkit.getPlayer(playername);

        Location firstPlayerLoc = this.player.getLocation();
        Location secondPlayerLoc = secondPlayer.getLocation();

        for (int i = 1; i <= 3; i++) {
            /*Bukkit.getServer().sendMessage(
                    Component.text("Свап через ", NamedTextColor.DARK_PURPLE)
                            .append(Component.text(i, NamedTextColor.GREEN))
                            .append(Component.text("...", NamedTextColor.DARK_PURPLE))
            );*/

            bossBar.setProgress(i * 0.33);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        new BukkitRunnable() {
            @Override
            public void run() {
                player.teleport(secondPlayerLoc);
                secondPlayer.teleport(firstPlayerLoc);
            }
        }.runTask(Bukkit.getPluginManager().getPlugin("TwitchChoice"));

        new BukkitRunnable() {
            @Override
            public void run() {
                bossBar.setTitle("§5§l...с §a§l" + playername + "§5§l!");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                bossBar.setVisible(false);
                bossBar.setProgress(0);
            }
        }.runTaskAsynchronously(Bukkit.getPluginManager().getPlugin("TwitchChoice"));

        /*Bukkit.getServer().sendMessage(
                Component.text(playername, NamedTextColor.AQUA).decoration(TextDecoration.BOLD, true)
                        .append(Component.text("!", NamedTextColor.DARK_PURPLE))

        );*/
    }

    public Effect getRandomEffect() {
        if (enchant) return config.positiveEffect[(int) (Math.random() * config.positiveEffect.length)];
        else return config.negativeEffects[(int) (Math.random() * config.negativeEffects.length)];
    }
}
