package ru.vladuska;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Localization {
    public static String playerPoll = "Выбирайте игрока!";
    public static String effectPoll = "Выбирайте эффект!";
    public static String swapPoll = "Выбирайте второго игрока!";

    public static TextComponent noChoice(String what) {
        return Component.text("Чат не выбрал " + what + " :(", NamedTextColor.DARK_RED).decoration(TextDecoration.BOLD, true);
    }

    public static List<String> effectChoices = new ArrayList<String>(Arrays.asList("Положительный", "Негативный", "Свап"));

    public static String effectTranslate(PotionEffectType effectType) {
        if (effectType.equals(PotionEffectType.SLOW)) return "МЕДЛИТЕЛЬНОСТЬ";
        if (effectType.equals(PotionEffectType.BLINDNESS)) return "СЛЕПОТА";
        if (effectType.equals(PotionEffectType.POISON)) return "ОТРАВЛЕНИЕ";
        if (effectType.equals(PotionEffectType.CONFUSION)) return "ТОШНОТА";
        if (effectType.equals(PotionEffectType.JUMP)) return "ПРЫГУЧЕСТЬ";
        if (effectType.equals(PotionEffectType.SPEED)) return "СКОРОСТЬ";
        if (effectType.equals(PotionEffectType.LUCK)) return "ПОЛЕТ"; // fly

        return "";
    }
}
