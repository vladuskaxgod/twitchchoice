package ru.vladuska;

import org.bukkit.potion.PotionEffectType;

public class Config {

    private static Config _instance;
    public static Config getInstance() {
        if (_instance == null) _instance = new Config();
        return _instance;
    }

    public static Runner runner = new Runner();
    public boolean isPollActive = false;

    private Config() {
        //this.runner = new Runner(); // doesn't work ?
    }

    public int pollDuration = 20;
    public double pollDelay = 40;

    public Effect[] negativeEffects = {
            new Effect(new PotionEffectType[] {PotionEffectType.SLOW}, 15, 4),
            new Effect(new PotionEffectType[] {PotionEffectType.BLINDNESS}, 15, 4),
            new Effect(new PotionEffectType[] {PotionEffectType.POISON}, 30, 0),
            new Effect(new PotionEffectType[] {PotionEffectType.CONFUSION}, 30, 8)
    };
    public Effect[] positiveEffect = {
            new Effect(new PotionEffectType[] {PotionEffectType.JUMP}, 20, 4),
            new Effect(new PotionEffectType[] {PotionEffectType.SPEED}, 20, 4),
            new Effect(new PotionEffectType[] {PotionEffectType.JUMP, PotionEffectType.SPEED}, 10, 4),
            new Effect(new PotionEffectType[] {PotionEffectType.LUCK}, 7, 4) // fly definition
    };
}
