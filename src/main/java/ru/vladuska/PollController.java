package ru.vladuska;

import okhttp3.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import static org.bukkit.Bukkit.getLogger;

public class PollController {

    private static PollController _instance;
    public static PollController getInstance() {
        if (_instance == null) _instance = new PollController();
        return _instance;
    }

    private final OkHttpClient httpClient = new OkHttpClient();
    private final JSONParser jsonParser = new JSONParser();
    private JSONObject reqBody = new JSONObject();
    private JSONArray choicesArray = new JSONArray();
    private Request request;

    private String lastPollId;
    private Config config = Config.getInstance();

    private Request requestBuilder(JSONObject body, boolean post) {

        Request.Builder builder = new Request.Builder()
                .url("https://api.twitch.tv/helix/polls")
                .addHeader("Cookie", "server_session_id=dfd4205113ad416fa6750d2e0c484772; twitch.lohp.countryCode=RU; unique_id=7flVQu1wvYUtwamqgENn1SUEGBdH4DnW; unique_id_durable=7flVQu1wvYUtwamqgENn1SUEGBdH4DnW")
                .addHeader("Client-Id", Creds.clientId)
                .addHeader("Authorization", "Bearer " + Creds.authorizationToken);

        if (post) {
            RequestBody formBody = RequestBody.create(
                    MediaType.parse("application/json"), body.toJSONString());
            return builder.post(formBody).build();
        }
        else return builder
                .url("https://api.twitch.tv/helix/polls?broadcaster_id=" + Creds.broadcasterId + "&id=" + lastPollId)
                .get()
                .build();
    }

    public void startNewPoll(String title, List<String> choices) {

        getLogger().warning("Starting poll...");

        reqBody = new JSONObject();
        choicesArray = new JSONArray();

        for (String choice : choices) {
            JSONObject choiceObj = new JSONObject();
            choiceObj.put("title", choice);
            choicesArray.add(choiceObj);
        }

        reqBody.put("broadcaster_id", Creds.broadcasterId);
        reqBody.put("title", title);
        reqBody.put("choices", choicesArray);
        reqBody.put("duration", config.pollDuration);

        getLogger().warning("Send POST req with body: " + reqBody.toJSONString());

        request = requestBuilder(reqBody, true);

        try (Response response = httpClient.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response + "\nMessage: " + response.body().string());

            JSONObject jsonRes = (JSONObject) jsonParser.parse(response.body().string());
            lastPollId = ((JSONObject) ((JSONArray) jsonRes.get("data")).get(0)).get("id").toString(); // xd meme fckn hate java
            if (lastPollId != null) getLogger().warning("Poll successfully started with id " + lastPollId);

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    public String getLastPollResult() {
        AtomicInteger maxVotes = new AtomicInteger();
        maxVotes.set(0);
        AtomicReference<String> choice = new AtomicReference<>();

        request = requestBuilder(null, false);

        try (Response response = httpClient.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

            JSONObject jsonRes = (JSONObject) jsonParser.parse(response.body().string());
            ((JSONArray) ((JSONObject) ((JSONArray) jsonRes.get("data")).get(0)).get("choices")).forEach(item -> {
                JSONObject obj = (JSONObject) item;
                if (Integer.parseInt(obj.get("votes").toString()) > maxVotes.get()) {
                    maxVotes.set(Integer.parseInt(obj.get("votes").toString()));
                    choice.set(obj.get("title").toString());
                }
            });

            getLogger().warning("Last poll result: " + choice.get());

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }

        return choice.get();
    }
}
