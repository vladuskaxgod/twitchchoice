package ru.vladuska;

import org.bukkit.potion.PotionEffectType;

public class Effect {
    public PotionEffectType[] types;
    public double duration;
    public int amplifier;

    public Effect(PotionEffectType[] types, double duration, int amplifier) {
        this.types = types;
        this.duration = duration;
        this.amplifier = amplifier;
    }
}