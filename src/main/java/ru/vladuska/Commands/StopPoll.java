package ru.vladuska.Commands;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import ru.vladuska.Config;

public class StopPoll implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!Config.getInstance().isPollActive) {
            Bukkit.getServer().sendMessage(Component.text("Голосование не запущено!", NamedTextColor.DARK_RED));
        }
        else {
            Config.runner.stop();
            Bukkit.getServer().sendMessage(Component.text("Остановка голосования!", NamedTextColor.DARK_RED));
        }
        return false;
    }
}
