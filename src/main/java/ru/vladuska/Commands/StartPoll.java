package ru.vladuska.Commands;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import ru.vladuska.Config;
import ru.vladuska.Runner;

public class StartPoll implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Bukkit.getServer().sendMessage(Component.text("Запуск голосования!", NamedTextColor.DARK_PURPLE));

        if (Config.getInstance().isPollActive) Bukkit.getServer().sendMessage(Component.text("Голосование уже запущено!", NamedTextColor.DARK_RED));
        else Config.runner.start();

        return false;
    }
}
