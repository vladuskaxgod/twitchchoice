package ru.vladuska;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

import static org.bukkit.Bukkit.getLogger;

public class Runner extends Thread {

    private PollController pollController = PollController.getInstance();
    private Config config = Config.getInstance();
    private String choice;

    public static BossBar bossBar = Bukkit.getServer().createBossBar("", BarColor.GREEN, BarStyle.SEGMENTED_20);

    public void run() {
        getLogger().warning("Runner starts!");
        config.isPollActive = true;

        while (true) {
            List<String> playernames = new ArrayList<>();

            for (Player player : Bukkit.getOnlinePlayers()) {
                playernames.add(player.getName());
            }

            pollController.startNewPoll(Localization.playerPoll, playernames);

            bossBar.setTitle("§5§lЧат выбирает игрока!");
            bossBar.setProgress(0);
            bossBar.setVisible(true);

            for (Player player : Bukkit.getOnlinePlayers()) {
                bossBar.addPlayer(player);
            }

            for (int i = 1; i <= config.pollDuration; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                bossBar.setProgress(i * 0.05);
            }

            bossBar.setVisible(false);

            choice = pollController.getLastPollResult();

            if (choice == null) {
                Bukkit.getServer().sendMessage(Localization.noChoice("игрока"));

                break;
            }

            Action action = new Action(choice);

            if (playernames.size() <= 2) {
                List<String> noSwap = new ArrayList<>(Localization.effectChoices);
                noSwap.remove(2);
                pollController.startNewPoll(Localization.effectPoll, noSwap);
            }
            else pollController.startNewPoll(Localization.effectPoll, Localization.effectChoices);

            //Bukkit.getServer().sendMessage(Component.text("Чат выбирает тип эффекта!", NamedTextColor.DARK_PURPLE));
            bossBar.setTitle("§5§lЧат выбирает тип эффекта!");
            bossBar.setProgress(0);
            bossBar.setVisible(true);

            for (int i = 1; i <= config.pollDuration; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                bossBar.setProgress(i * 0.05);
            }

            bossBar.setVisible(false);

            choice = pollController.getLastPollResult();

            if (choice == null) {
                Bukkit.getServer().sendMessage(Localization.noChoice("эффект"));
                Thread.currentThread().stop();
            }

            if (pollController.getLastPollResult().equals(Localization.effectChoices.get(0)))
                action.setEnchant(true);
            else if (pollController.getLastPollResult().equals(Localization.effectChoices.get(1)))
                action.setEnchant(false);
            else {
                playernames.remove(action.getPlayername());
                /*pollController.startNewPoll(Localization.swapPoll, playernames);
                Bukkit.getServer().sendMessage(Component.text("Чат выбрал свапнуть игроков! Сейчас узнаем, кто поменяется местами...", NamedTextColor.DARK_PURPLE));

                try {
                    Thread.sleep((config.pollDuration + 1) * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                choice = pollController.getLastPollResult();

                if (choice == null) {
                    Bukkit.getServer().sendMessage(Localization.noChoice("второго игрока"));
                    Thread.currentThread().stop();
                }*/

                choice = playernames.get((int) (Math.random() * playernames.size()));

                action.swap(choice);
            }

            for (double i = 1; i <= config.pollDelay; i++) {
                if (i >= 40) {
                    bossBar.setVisible(true);
                    bossBar.setTitle("§5§lСледующее голосование через §a§l" + (int) (config.pollDelay - i) + " §5§lсекунд!");
                    bossBar.setProgress(i * (1 / config.pollDelay));
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
