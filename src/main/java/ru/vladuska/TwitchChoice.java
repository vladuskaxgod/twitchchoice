package ru.vladuska;

import org.bukkit.plugin.java.JavaPlugin;
import ru.vladuska.Commands.StartPoll;
import ru.vladuska.Commands.StopPoll;

public final class TwitchChoice extends JavaPlugin {

    @Override
    public void onEnable() {
        this.getCommand("startpoll").setExecutor(new StartPoll());
        this.getCommand("stoppoll").setExecutor(new StopPoll());
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
